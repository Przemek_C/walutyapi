package com.example.rent.walutyapi;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-06-26.
 */

public class ShowCurrency extends AsyncTask<Void, Void, List<String>> {

    public static final String CODE = "code";
    public static final String CURRENCY = "currency";
    public static final String RATES = "rates";

    @Override
    protected List<String> doInBackground(Void... params) {
        String[] tables = {"a", "b", "c"};
        List<String> stringArrayList = new ArrayList<>();
        try {
            for (String table : tables) {
                URL url = new URL("http://api.nbp.pl/api/exchangerates/tables/" + table + "/");
                HttpURLConnection http;
                http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("GET");
                http.connect();
                int responseCode = http.getResponseCode();
                if (responseCode == 200) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(http.getInputStream()));
                    String line = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    stringArrayList.addAll(parse(stringBuilder));
                } else {
                    System.out.println("Tabela " + table + "nie obsłużona");
                }
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return stringArrayList;
    }

    private List<String> parse(StringBuilder stringBuilder) throws JSONException {
        List<String> result = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(stringBuilder.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = (JSONObject) jsonArray.get(i);
            JSONArray array = (JSONArray) obj.get(RATES);
            for (int j = 0; j < array.length(); j++) {
                JSONObject objCurr = (JSONObject) array.get(j);
//                            System.out.print(j + 1 + ". " + objCurr.get(CODE));
//                            System.out.print(": ");
//                            System.out.println(objCurr.get(CURRENCY));
                result.add(objCurr.get(CODE).toString());
            }
        }
        return result;
    }
}
