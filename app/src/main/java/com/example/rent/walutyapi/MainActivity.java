package com.example.rent.walutyapi;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button)
    Button button;
    @BindView(R.id.listView)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void showList() throws ExecutionException, InterruptedException {
        List<String> res = new ShowCurrency().execute().get();
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, res));
        CurrenciesList currenciesList = new CurrenciesList(getApplicationContext(), res);
        ListView viewById = (ListView) findViewById(R.id.listView);
        viewById.setAdapter(currenciesList);
    }
}
